# Freshly Brewed Virtualsquare
Automatically create daily disk images to test the latest developments of Debian sid
and virtualsquare with [the dedicated script](https://github.com/virtualsquare/freshly_brewed_virtualsquare).

The script `brew_v2` downloads the latest version of SID/unstable from the 
[Debian Official Cloud Images](http://cdimage.debian.org/cdimage/cloud/)
and customizes it to be used as a Tutorial environment for the 
[Virtualsquare projects](http://wiki.virtualsquare.org/#!index.md).

# LATEST DAILY BUILD

[Download from here](https://gitlab.com/CarloDePieri/virtualsquare-brewer/-/jobs/artifacts/main/download?job=v2-daily-build)

# LAST WEEK 7 BUILDS

Can be downloaded as artifacts of the [sheduled pipeline jobs](https://gitlab.com/CarloDePieri/virtualsquare-brewer/-/jobs).
